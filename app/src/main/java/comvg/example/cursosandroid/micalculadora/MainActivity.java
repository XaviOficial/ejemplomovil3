package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtRes;
    private Button btnSumar, btnRestar, btnMulti, btnDiv, btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEventos();

        }

    public void initComponents(){
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtRes = (EditText) findViewById(R.id.txtRes);
        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMulti = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDivi);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        setEventos();
    }

    public void setEventos(){
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSuma:
                if(txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar un número", Toast.LENGTH_SHORT).show();
                }
                else{
                    sumar();
                }
                break;

            case R.id.btnResta:
                if(txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar un número", Toast.LENGTH_SHORT).show();
                }
                else{
                    restar();
                }
                break;

            case R.id.btnMult:
                if(txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar un número", Toast.LENGTH_SHORT).show();
                }
                else{
                    multiplicar();
                }
                break;

            case R.id.btnDivi:
                if(txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar un número", Toast.LENGTH_SHORT).show();
                }
                else{
                    dividir();
                }
                break;

            case R.id.btnLimpiar:
                limpiar();
                break;

            case R.id.btnCerrar:
                cerrar();
                break;
        }
    }

    public void sumar(){
        op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
        txtRes.setText(String.valueOf(op.suma()));
    }

    public void restar(){
        op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
        txtRes.setText(String.valueOf(op.resta()));
    }

    public void multiplicar(){
        op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
        txtRes.setText(String.valueOf(op.mult()));
    }

    public void dividir(){
        op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
        txtRes.setText(String.valueOf(op.div()));
    }

    public void limpiar(){
        txtNum1.setText("");
        txtNum1.requestFocus();
        txtNum2.setText("");
        txtRes.setText("");
    }

    public void cerrar(){
        finish();
    }
}
